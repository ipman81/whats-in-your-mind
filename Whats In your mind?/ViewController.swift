//
//  ViewController.swift
//  Whats In your mind?
//
//  Created by Adrian Watzlawczyk on 25/05/15.
//  Copyright (c) 2015 Adrian Watzlawczyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    
    var clicksCounter = 0
    
    @IBAction func next(sender: AnyObject) {
        clicksCounter++

        if clicksCounter == 1 {
            
            myLabel.text = "Multiply it with 5"
            
        }else if clicksCounter == 2 {
            
            myLabel.text = "Now add 5 to the total"
            
        }else if clicksCounter == 3 {
            
            myLabel.text = "Multiple the answer with 2"
        }else if clicksCounter == 4 {
            
            myLabel.text = "Now add 2 to the total"
            
        }else if clicksCounter == 5 {
            
            myLabel.text = "What is the total"
            textField.hidden = false
        }else if clicksCounter == 6 {
            if textField.text != "" {
                var input:Int? = textField.text.toInt()
                
                var endNumber = ((((input! - 2) / 2) - 5) / 5)
                
                myLabel.text = "\(endNumber)"
                
                sender.setTitle("Play?", forState: UIControlState.Normal)
            }else{
                var alert = UIAlertController(title: "Ooops", message: "please provide total", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                clicksCounter = 5
            }
            
            self.view.endEditing(true)
            
    
        }
        
        if clicksCounter  > 6 {
            myLabel.text = "What is the total"
            textField.text = ""
            clicksCounter = 0
            textField.hidden = true
            sender.setTitle("Next", forState: .Normal)
            self.view.endEditing(false)
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.text = "Think of a number"
        textField.text = ""
        textField.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

